%search(Elem,List)
search(X,[X|_]).
search(X,[_|Xs]):-search(X,Xs).

% search2(Elem,List)
% looks for two consecutive occurrences of Elem
search2(X,[X,X|_]).
search2(X,[_|Xs]):-search2(X,Xs).

% search_two(Elem,List)
% looks for two occurrences of Elem with an element in between!
search_two(X,[X,_,X|_]).
search_two(X,[_|Xs]):-search_two(X,Xs).

% search_anytwo(Elem,List)
% looks for any Elem that occurs two times
search_anytwo(X,[X|T]):-search(X,T).
search_anytwo(X,[_|T]):-search_anytwo(X,T).

% size(List,Size)
% Size will contain the number of elements in List
size_number([],0).
size_number([_|T],M) :- size_number(T,N), M is N+1.

% size(List,Size)
% Size will contain the number of elements in List
size([],zero).
size([_|T],s(M)) :- size(T,M).

%sum(List,Sum)
sum([],0).
sum([H|T],M):-sum(T,N), M is H+N.

% average(List,Average)
% it uses average(List,Count,Sum,Average)
% average(List,Average)
% it uses average(List,Count,Sum,Average)
average(List,A) :- average(List,0,0,A).
average([],C,S,A) :- A is S/C.
average([X|Xs],C,S,A) :-
C2 is C+1,
S2 is S+X,
average(Xs,C2,S2,A).

% max(List,Max)
% Max is the biggest element in List
% Suppose the list has at least one element
max([X],X).
max([X|Xs],M) :- 
max(Xs,Y),
( X>=Y, M = X;
Y>X, M = Y
).

% same(List1,List2)
% are the two lists the same?
same([],[]).
same([X|Xs],[X|Ys]):- same(Xs,Ys).

% all_bigger(List1,List2)
% all elements in List1 are bigger than those in List2, 1 by 1
% example: all_bigger([10,20,30,40],[9,19,29,39]).
all_bigger([],[]).
all_bigger([X|Xs],[Y|Ys]):- 
X>Y,
all_bigger(Xs,Ys).

% sublist(List1,List2)
% List1 should be a subset of List2
% example: sublist([1,2],[5,3,2,1]).
% search(Elem,List)
sublist([],List).
sublist([X|Xs],List) :- search(X,List).

% seq(N,List)
% example: seq(5,[0,0,0,0,0]).
seq(0,[]).
seq(N,[0|T]):- N > 0, N2 is N-1, seq(N2,T).

% seqR(N,List)
% example: seqR(4,[4,3,2,1,0]).
seqR(0,[0]).
seqR(N,[X|T]):- N > 0, N2 is N-1, X is N, seqR(N2,T).

% seqR2(N,List)
% example: seqR2(4,[0,1,2,3,4]).
last([],N,[N]).
last([H|T],N,[H|M]) :- last(T,N,M).

seqR2(0,L).
seqR2(N,L) :- last(A,N,L), C is N-1, seqR2(C,A).

% inv(List,List)
% example: inv([1,2,3],[3,2,1]).
inv([],[]).
inv([H|T],R) :- inv(T,RT), append(RT,[H],R).

% double(List,List)
% suggestion: remember predicate append/3
% example: double([1,2,3],[1,2,3,1,2,3]).
%double([],[]).
double(X,L) :- append(X,X,L).

% times(List,N,List)
% example: times([1,2,3],3,[1,2,3,1,2,3,1,2,3]).
times(X,0,[]).
times(X,N,L) :- N>0, C is N-1, times(X,C,LT), append(LT,X,L).

% proj(List,List)
% example: proj([[1,2],[3,4],[5,6]],[1,3,5]).
proj([],[]).
proj([[H|_]|T],L) :- proj(T,LT), append([H],LT,L).