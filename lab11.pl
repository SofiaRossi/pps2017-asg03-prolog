% dropAny(?Elem,?List,?OutList)
dropAny(X,[X|T],T).
dropAny(X,[H|Xs],[H|L]):-dropAny(X,Xs,L).

% dropFirst(?Elem,?List,?OutList)
dropFirst(X,[X|T],T) :- !.
dropFirst(X,[H|Xs],[H|L]):-dropFirst(X,Xs,L).

% dropLast(?Elem,?List,?OutList)
dropLast(X,[H|Xs],[H|L]) :- dropLast(X,Xs,L), !.
dropLast(X,[X|T],T).

% dropAll(?Elem,?List,?OutList)
dropAll(X, [], []) :- !.
dropAll(X, [X|Xs], Y) :- dropAll(X, Xs, Y), !.
dropAll(X, [T|Xs], [T|Y]) :- dropAll(X, Xs, Y).

% fromList(+List,-Graph)
fromList([_],[]).
fromList([H1,H2|T],[e(H1,H2)|L]):- fromList([H2|T],L).

% fromCircList(+List,-Graph)
fromCircList([E],[e(E,H)],H).
fromCircList([H1,H2|T],[e(H1,H2)|L],H):- fromCircList([H2|T],L,H).
fromCircList([H1,H2|T],[e(H1,H2)|L]):- fromCircList([H2|T],L,H), H is H1.

% dropAll(?Elem,?List,?OutList)
dropAllG(N, [], []) :- !.
dropAllG(N, [e(N,_)|Gs], Y) :- !, dropAllG(N, Gs, Y).
dropAllG(N, [e(T,_)|Gs], [e(T,_)|Y]) :- dropAllG(N, Gs, Y).

% dropNode(+Graph, +Node, -OutGraph)
% drop all edges starting and leaving from a Node
% use dropAll defined in 1.1
dropNode(G,N,O):- dropAll(e(N,_),G,G2),
dropAll(e(_,N),G2,O).

% reaching(+Graph, +Node, -List)
% all the nodes that can be reached in 1 step from Node
% possibly use findall, looking for e(Node,_) combined
% with member(?Elem,?List)
reaching(G,N,L) :- findall(D,member(e(N,D),G),L).

% anypath(+Graph, +Node1, +Node2, -ListPath)
% a path from Node1 to Node2
% if there are many path, they are showed 1-by-1
anypath(G,O,D,L) :- member(e(O,B),G), anypath(G,B,D,NL), append([e(O,B)],NL,L).
anypath(G,O,D,L) :- member(e(O,D),G), append([],[e(O,D)],L),!.

% allreaching(+Graph, +Node, -List)
% all the nodes that can be reached from Node
% Suppose the graph is NOT circular!
% Use findall and anyPath!
allreaching(G,N,L) :- findall(D, anypath(G,N,D,_),L).
